import {
  validateInt,
  validateName,
  validateCoordRnage,
  calculateDistance,
} from "../helpers.js";

export class Customer {
  constructor(name, id, x, y) {
    this.name = validateName(name);
    this.id = validateInt(id);
    this._x = validateCoordRnage(validateInt(x));
    this._y = validateCoordRnage(validateInt(y));
    this.orders = [];
  }

  get x() {
    return this._x;
  }

  set x(value) {
    this._x = validateCoordRnage(validateInt(value));
  }

  get y() {
    return this._y;
  }

  set y(value) {
    this._y = validateCoordRnage(validateInt(value));
  }

  addOrder(order) {
    this.orders.push(order);
  }

  async deliverCustomersOrders(drone, order, timeAtWerehouse = 5) {
    let distance = calculateDistance(drone.x, drone.y, this.x, this.y);

    let minutesForDelivery = distance + timeAtWerehouse;

    this.orders.filter((o) => {
      if (o == order) {
        order.status = "delivering";
      }
    });

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.orders.filter((o) => {
          if (o == order) {
            order.status = "delivered";
          }
        });
        resolve(minutesForDelivery);
      }, 100 * minutesForDelivery);
    });
  }
}
