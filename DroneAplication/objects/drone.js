import { calculateDistance } from "../helpers.js";
import { validateName, validateInt, validateCoordRnage } from "../helpers.js";

export class Drone {
  constructor(name, x, y, capacity, consumption) {
    this.name = validateName(name);
    this._x = validateCoordRnage(validateInt(x));
    this._y = validateCoordRnage(validateInt(y));
    this.capacity = capacity;
    this.consumption = consumption;
    this.batteryLife = 100;
  }

  get x() {
    return this._x;
  }

  set x(value) {
    this._x = validateCoordRnage(validateCoord(value));
  }

  get y() {
    return this._y;
  }

  set y(value) {
    this._y = validateCoordRnage(validateCoord(value));
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = validateName(value);
  }

}
