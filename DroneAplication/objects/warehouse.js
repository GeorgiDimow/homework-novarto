import { Drone } from "./drone.js";
import {
  validateInt,
  validateName,
  validateCoordRnage,
  calculateDistance,
} from "../helpers.js";

export class Warehouse {
  constructor(name, x, y) {
    this._x = validateCoordRnage(validateInt(x));
    this._y = validateCoordRnage(validateInt(y));
    this._name = validateName(name);
    this._drones = [];
    this.customers = [];
  }

  get x() {
    return this._x;
  }

  set x(value) {
    this._x = validateCoordRnage(validateInt(value));
  }

  get y() {
    return this._y;
  }

  set y(value) {
    this._y = validateCoordRnage(validateInt(value));
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = validateString(value);
  }

  addCustomer(customer) {
    this.customers.push(customer);
  }

  addOrder(order) {
    this._orders.push(order);
  }

  addDrone(name, drone) {
    this._drones.push(
      new Drone(name, this._x, this._y, drone.capacity, drone.consumption)
    );
  }

  async getDroneFormDelivery(customer, drone) {
    let distance = calculateDistance(drone.x, drone.y, customer.x, customer.y);

    setTimeout(() => this._drones.push(drone), 100 * distance);

    return distance;
  }

  async checkBatteryDrone(drone, cusotmer) {
    let time = calculateDistance(this._x, this._y, cusotmer.x, cusotmer.y);

    let oldBatteryLife = drone.batteryLife;
    this.calculateDroneBattery(drone, time * drone.consumption);

    if (this.batteryLife <= 0) {
      drone.batteryLife = oldBatteryLife;
      chargingDorne(drone);
      return false;
    }

    return true;
  }

  async chargingDorne(drone) {
    setTimeout(() => (drone.batteryLife = 100), 2000);
  }

  calculateDroneBattery(drone, consumptionMade) {
    const persentage =
      ((drone.capacity - consumptionMade) / drone.capacity) * 100;
    drone.batteryLife = persentage;
  }

  async deliverOredrsToAllCustomers() {
    let timeForAllOrders = [];

    for (let customer of this.customers) {
      let backTime = 0;

      if (!customer.orders.length) {
        continue;
      }

      for (let i = 0; i < customer.orders.length - 1; i++) {
        let order = customer.orders[i];
        if (order.status == "not deliverd") {
          const drone = this._drones.shift();
          if (this.checkBatteryDrone(drone, customer)) {
            if (!this._drones.length) {
              backTime = await this.getDroneFormDelivery(customer, drone);
            }

            customer.deliverCustomersOrders(drone, order).then((time) => {
              this.getDroneFormDelivery(customer, drone);

              timeForAllOrders.push(backTime + time);
            });
          } else {
            this._drones.push(drone);
            i--;
          }
        }
      }

      const lastOrder = customer.orders[customer.orders.length - 1];

      while (true) {
        const drone = this._drones.shift();
        if (this.checkBatteryDrone(drone, customer)) {
          if (lastOrder.status == "not deliverd") {
            if (!this._drones.length) {
              backTime = await this.getDroneFormDelivery(customer, drone);
            }
            await customer
              .deliverCustomersOrders(drone, lastOrder)
              .then((time) => {
                timeForAllOrders.push(backTime + time);
                this.getDroneFormDelivery(customer, drone);
              });
          }
          break;
        } else {
          this._drones.push(drone);
        }
      }
    }

    const result = Math.max(...timeForAllOrders);
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(result);
      }, 100 * result);
    });
  }
}
