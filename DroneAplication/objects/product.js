import { validateInt, validateName } from "../helpers.js";

export class Product{
    constructor(name, quantity){
        this._name = validateName(name);
        this._quantity = validateInt(quantity);
    }
    
    get quantity(){
        return this._quantity;
    }

    set quantity(value){
        this._quantity = validateInt(value);
    }

    get name(){
        return this._name;
    }

    set name(value){
        this._name = validateName(value);
    }

}