import { validateName, validateInt, calculateDistance } from "../helpers.js";

export class Order {
  constructor(name, customerId, productList) {
    this.name = validateName(name);
    this.customerId = validateInt(customerId);
    this.productList = productList;
    this.status = "not deliverd"
  }
}
