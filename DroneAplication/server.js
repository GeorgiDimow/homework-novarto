import { WebSocketServer } from "ws";
import { ApplicationController } from "./applicationController.js";
import data from "./data.json" assert { type: "json" };
import { configuration } from "./helpers.js";

export const app = new ApplicationController();
const wss = new WebSocketServer({ port: 8080 });

configuration(app);

wss.on("connection", async function connection(ws) {
  ws.on("message", (message) => {
    const messageData = JSON.parse(message);

    if (messageData.action == "addProduct") {
      app.addProduct(messageData.product, messageData.quantity);
    }
    if (messageData.action == "addOrder") {
      app.addOrder(
        messageData.name,
        messageData.customerId,
        messageData.productList
      );
    }
    if (messageData.action == "addWerehouse") {
      //TODO
    }
    if (messageData.action == "addCustomer") {
      //TODO
    }
    if (messageData.action == "addDrone") {
      //TODO
    }
    if (messageData.action == "deliverAllOrders") {
      app.deliverAllOrders().then((result) => {
        ws.send(`Total time: ${Math.round(result)} minutes`);
      });
    }
    if (messageData.action == "status") {
      ws.send(app.statusOredrsByCustomer(messageData.customerId).toString());
    }
  });
});
