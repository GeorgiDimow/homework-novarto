import WebSocket from "ws";

const ws = new WebSocket("ws://localhost:8080");

ws.on("open", function open() {
  const messageForAddingOrders1 = {
    action: "addOrder",
    name: "salad",
    customerId: 1,
    productList: {
      tomatoes: 5,
      cucumber: 5,
      cheese: 1,
    },
  };

  const messageForAddingOrders2 = {
    action: "addOrder",
    name: "order",
    customerId: 1,
    productList: {
      tomatoes: 5,
      cucumber: 5,
      cheese: 1,
    },
  };

  const messageStatusOrdersOfACustomer = {
    action: "status",
    customerId: 1,
  };

  const messageDeliverAllOrders = {
    action: "deliverAllOrders",
  };

  ws.send(JSON.stringify(messageForAddingOrders1));
  ws.send(JSON.stringify(messageForAddingOrders2));
  ws.send(JSON.stringify(messageDeliverAllOrders));

  setInterval(() => {
    ws.send(JSON.stringify(messageStatusOrdersOfACustomer));
  }, 10000);

  const messageForAddingWarehouses = {};//TODO

  const messageForAddingCustomers = {};//TODO

  const messageForAddingProducts = {};//TODO
});

ws.on("message", (message) => {
  console.log("Server response:\n", message.toString());
});
