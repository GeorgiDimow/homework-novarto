import data from "./data.json" assert { type: "json" };

export function sortCoordinatesByProximity(objects, targetOject) {
  objects.sort((oject1, oject2) => {
    const x1 = oject1.x;
    const y1 = oject1.y;
    const x2 = oject2.x;
    const y2 = oject2.y;
    const dist1 = calculateDistance(x1, y1, targetOject.x, targetOject.y);
    const dist2 = calculateDistance(x2, y2, targetOject.x, targetOject.y);
    return dist1 - dist2;
  });
  return objects;
}

export function calculateDistance(x1, y1, x2, y2) {
  const distance = Math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2);
  return distance;
}

export function validateName(value) {
  if (typeof value !== 'string') {
    throw new Error(`Name must be a string.`);
  }
  return value;
}

export function validateInt(value) {
  if (!Number.isInteger(value)) {
    throw new Error(`Coordinate must be an integer.`);
  }
  return value;
}

export function validateCoordRnage(value) {
  if (!(data["map-top-right-coordinate"]['x'] > value > 0)) {
      throw new Error(`Coordinate must be beteew 0 and 280.`);
  }
  return value;
}

export function configuration(app){
  for (let i in data["products"]) {
    app.addProduct(i, data["products"][i]);
  }
  
  for (let warehouse of data["warehouses"]) {
    let name = warehouse.name;
    let x = warehouse.x;
    let y = warehouse.y;
    app.addWerehouse(name, x, y);
  }
  
  for (let customer of data["customers"]) {
    let name = customer.name;
    let id = customer.id;
    let x = customer.coordinates.x;
    let y = customer.coordinates.y;
    app.addCustomerToTheClosestWerehouse(name, id, x, y);
  }
  
  // only if a order is left from the last day
  // let a = "a";
  // for (let order of data["orders"]) {
  //   let customerId = order.customerId;
  //   let productList = order.productList;
  //   app.addOrder(a, customerId, productList);
  //   a += "a";
  // }
  
  let name = "Nia";
  for (let warehouse of app.werehouses) {
    for (let drone of data["typesOfDrones"]) {
      warehouse.addDrone(name, drone);
      name += "1";
    }
  }
  
}

