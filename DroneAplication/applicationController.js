import { Product } from "./objects/product.js";
import { Warehouse } from "./objects/warehouse.js";
import { Order } from "./objects/order.js";
import { Drone } from "./objects/drone.js";
import { Customer } from "./objects/customer.js";
import { sortCoordinatesByProximity } from "./helpers.js";

export class ApplicationController {
  constructor() {
    this.werehouses = [];
    this.products = [];
    this.customers = [];
  }

  addProduct(name, quantity) {
    if (!this.products.filter((p) => p == new Product(name, quantity)).length) {
      this.products.push(new Product(name, quantity));
    }
  }

  addWerehouse(name, x, y) {
    if (!this.werehouses.filter((w) => w == new Warehouse(name, x, y)).length) {
      this.werehouses.push(new Warehouse(name, x, y));
    }
  }

  addCustomerToTheClosestWerehouse(name, id, x, y) {
    const customer = new Customer(name, id, x, y);

    const werehouse = sortCoordinatesByProximity(this.werehouses, customer)[0];

    if (!werehouse.customers.filter((c) => c == customer).length) {
      this.customers.push(customer);
      werehouse.addCustomer(customer);
    }
  }

  addOrder(name, customerId, productList) {
    let customer = null;

    for (let c of this.customers) {
      if (c.id == customerId) {
        customer = c;
        break;
      }
    }

    const order = new Order(name, customerId, productList);

    for (let i in productList) {
      this.products.filter((p) => {
        if ((p.name == i)) {
          p.quantity -= productList[i];
        }
      });
    }
    if (!customer.orders.filter((o) => o == order).length) {
      customer.addOrder(order);
    }
  }

  async deliverAllOrders() {
    let totalTime = 0;
    let warehouseTimes = [];
    for (let warehouse of this.werehouses) {
      let timeForAWarehouse = 0;
      await warehouse.deliverOredrsToAllCustomers().then((result) => timeForAWarehouse= result);
      warehouseTimes.push(timeForAWarehouse);
    }
    totalTime = Math.max(...warehouseTimes);
    return totalTime;
  }

  statusOredrsByCustomer(customerID) {
    const cusotmer = this.customers.filter((c) => c.id == customerID)[0];
    const orders = cusotmer.orders;
    let output = '';
    for (let order of orders) {
      output += `${order.name} - ${order.status}\n`;
    }
    return output;
  }
}
