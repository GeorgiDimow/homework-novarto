Georgi Dimov's homework

Information about the homework:
1. client.js is a client application that sends requests to a server.
 - It can check the status of all orders by a given customer.
 - It can add orders in real-time.
 - It can request the delivery of all orders.

2. server.js is a server application that responds to a client.
 - It uses an app controller to handle requests.

3. applicationController.js is an implementation of the main functionalities of the software:
 - It adds products.
 - It adds warehouses.
 - It assigns customers to the closest warehouse.
 - It adds orders.
 - It delivers all orders.
 - It checks the status of orders by a given customer.

4. warehouse.js is an implementation of calculations and logic:
 - It checks and charges the drones.
 - It navigates the drones.
 - It manages customer deliveries.

5. The following files are objects used for the purposes of this application:
 - customer.js
 - drone.js
 - order.js
 - product.js

6. helpers.js contains all the necessary tools to work with data:
 - Validation
 - Common calculations
 - Configuration of the app
